#!/bin/sh
# Assign the filename
filename=public/index.html
version=$(date +%s)

sed -i -E "s/data.js\?version=[0-9]+/data.js?version=$version/g" $filename
